#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <DHT.h>
#include <Ticker.h>
#include "NTPtimeESP.h"
#include "ArduinoJson.h"
#include "topic.h"
#include "time.h"

#define max_device_temperature 90
#define min_device_temperature 60

#define max_battery 13
#define min_battery 11

#define max_rpm 2000
#define min_rpm 1300

uint32_t environment_temp_freq = 300000;//interval to push data to broker
uint32_t environment_humid_freq = 300000;
uint32_t device_temp_freq = 300000;
uint32_t device_location_freq = 300000;
uint32_t battery_rate_voltage_freq = 300000;
uint32_t vehicle_rpm_freq = 300000;

const char* ssid = "Viet Quy";
const char* password = "vietquy160591";
const char* mqtt_server = "broker.emqx.io";
const double time_sleeping = 30e6;//time sleeping
const String latitude = "20.9913389";
const String longitude = "105.8375117";
WiFiClient espClient;
PubSubClient client (espClient);
DHT dht11 (2, DHT11);
NTPtime NTPch("pool.ntp.org");
strDateTime dateTime;
StaticJsonDocument<200> doc;
struct tm* now;
time_t rawTime;

float temp = 0.0;
float humidity = 0.0;
float newTemp;
float newHumidity;
unsigned int previousMillis;
int statusCode;

void setup_wifi (void);
void callback (char* topic, byte* payload, unsigned int length);
void reconnect (void);
void send_environment_temp();
void send_environment_humid();
void send_device_temp();
void send_device_location();
void get_geo_date_time_json();
void get_local_time();
void send_rate_voltage ();
void send_rpm ();
String iso8061_standard(strDateTime dateTime);
String iso8061_field_standard(byte field);


Ticker send_environemt_temp_task;
Ticker send_environment_humid_task;
Ticker send_device_temp_task;
Ticker send_device_location_task;
Ticker send_rate_voltage_task;
Ticker send_rpm_task;

void setup() {
  Serial.begin (9600);
  Serial.println("Hello I'm back");
  client.setServer (mqtt_server,1883);
  client.setCallback (callback);

  setup_wifi ();

  dht11.begin ();

  NTPch.setSendInterval(60);

  send_environemt_temp_task.attach_ms(environment_temp_freq, send_environment_temp);
  send_environment_humid_task.attach_ms(environment_humid_freq, send_environment_humid);
  send_device_temp_task.attach_ms(device_temp_freq, send_device_temp);
  send_device_location_task.attach_ms(device_location_freq, send_device_location);
  send_rate_voltage_task.attach_ms(battery_rate_voltage_freq, send_rate_voltage);
  send_rpm_task.attach_ms(vehicle_rpm_freq, send_rpm);

  //subscribe some topics here
  // Serial.println("Time to sleep");
  // client.publish ("mandevices/DoAnhDung_20181421/$state","sleep",true);
  // ESP.deepSleep(time_sleeping);
  // delay(100);

}

void loop() {
  client.loop();
  dateTime = NTPch.getNTPtime(7.0,0);
   if (dateTime.valid)
  {
    get_geo_date_time_json();
  }
  newTemp = dht11.readTemperature ();
  newHumidity = dht11.readHumidity ();
}

void setup_wifi ()
{
  delay (10);
  Serial.println ();
  Serial.print ("Connecting to ");
  Serial.print (ssid);
  WiFi.mode (WIFI_STA);
  WiFi.begin (ssid, password);
  reconnect ();
}

void callback (char* topic, byte* payload, unsigned int length)
{
  String payload_content;
  Serial.print ("Message arrived [");
  Serial.print (topic);
  Serial.print ("]");
  for (unsigned int i=0; i < length; i++)
  {
    Serial.print ((char)payload [i]);
    payload_content += (char)payload[i];
  }
  Serial.println ();

  if (!strcmp(topic,node_device_properties_temperature_set_freq))
  {
    Serial.print("device temperature freq change from ");
    Serial.print(device_temp_freq);
    device_temp_freq = payload_content.toInt() * 1000; 
    Serial.print(" to ");
    Serial.println(device_temp_freq);
    client.publish(node_device_properties_temperature_freq, payload_content.c_str(), true);
    send_device_temp_task.attach_ms_scheduled(device_temp_freq, send_device_temp);
    
  }
  else
  if (!strcmp(topic,node_device_properties_location_set_freq))
  {
    Serial.print("device location freq change from ");
    Serial.print(device_location_freq);
    device_location_freq = payload_content.toInt() * 1000; 
    Serial.print(" to ");
    Serial.println(device_location_freq);
    client.publish (node_device_properties_location_freq, payload_content.c_str(), true);
    send_device_location_task.attach_ms_scheduled(device_location_freq, send_device_location);
   
  }
  else
  if (!strcmp(topic,node_battery_properties_rate_voltage_set_freq))
  {
    Serial.print("battery rate voltage freq change from ");
    Serial.print(battery_rate_voltage_freq);
    battery_rate_voltage_freq = payload_content.toInt() * 1000; 
    Serial.print(" to ");
    Serial.println(battery_rate_voltage_freq);
    client.publish(node_battery_properties_rate_voltage_freq, payload_content.c_str(), true);
    send_rate_voltage_task.attach_ms(battery_rate_voltage_freq, send_rate_voltage);
  }
  else
  if (!strcmp(topic,node_vehicle_properties_rpm_set_freq))
  {
    Serial.print("vehicle rpm freq change from ");
    Serial.print(vehicle_rpm_freq);
    vehicle_rpm_freq = payload_content.toInt() * 1000; 
    Serial.print(" to ");
    Serial.println(vehicle_rpm_freq);
    client.publish(node_vehicle_properties_rpm_freq, payload_content.c_str(), true);
    send_rpm_task.attach_ms(vehicle_rpm_freq, send_rpm);
  }
  else
  if (!strcmp(topic,node_environment_properties_temperatue_freq_set))
  {
    Serial.print("environement temperature freq change from ");
    Serial.print(environment_temp_freq);
    environment_temp_freq = payload_content.toInt() * 1000; 
    Serial.print(" to ");
    Serial.println(environment_temp_freq);
    client.publish(node_environment_properties_temperature_freq, payload_content.c_str(), true);
    send_environemt_temp_task.attach_ms(environment_temp_freq, send_environment_temp);
  }
  else
  if (!strcmp(topic,node_environment_properties_humidity_freq_set))
  {
    Serial.print("environment humid freq change from ");
    Serial.print(environment_humid_freq);
    environment_humid_freq = payload_content.toInt() * 1000; 
    Serial.print(" to ");
    Serial.println(environment_humid_freq);
    client.publish(node_environment_properties_humidity_freq, payload_content.c_str(), true);
    send_environment_humid_task.attach_ms(environment_humid_freq, send_rpm);
  }
  
}

void reconnect (void) 
{
if (WiFi.status() != WL_CONNECTED)
{
  while ((WiFi.status() != WL_CONNECTED))
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.println ();
  Serial.println ("Wifi Connected");
  Serial.println ("IP address: ");
  Serial.println (WiFi.localIP ());
  Serial.println(WiFi.macAddress ());
}

while (!client.connected ())
{
  Serial.print ("Attemping MQTT connection...");
  //Create a client ID
  String clientID = "ESP8266Client-";
  clientID += String (random(0xffff), HEX);
  
  //Attempt to connect
  if (client.connect (clientID.c_str(),"","",device_state,1,true, "disconnected")) 
  {
    Serial.println ("connected");

                    /*************DoAnhDung_20181421***************/
    //publish attributes of device
    client.publish (device_homie_version,"4.0.0",true);
    client.publish (device_name,"DoAnhDung_20181421",true);
    client.publish (device_state,"ready",true);
    client.publish (device_nodes,"environment,device",true);
    client.publish (device_extension,"",true);
    client.publish (device_implementation,"esp12-e",true);

                      /**********Environment Node************/
    //once connected to broker, publish attributes of DHT11 node when DHT11 node is ready
    if ((dht11.readTemperature() && dht11.readHumidity ()))
    {
      client.publish (node_environment_name,"Thông số môi trường",true); 
      client.publish (node_environment_type,"dht11",true);
      client.publish (node_environment_properties,"temperature,humidity",true);

      //publish property attributes of temperature node
      client.publish (node_environment_properties_temperature_name,"Nhiệt Độ Môi Trường",true);
      client.publish (node_environment_properties_temperature_datatype,"float",true);
      client.publish (node_environment_properties_temperature_unit,"°C",true);
      client.publish (node_environment_properties_temperature_format,"0:50",true);
      client.publish (node_environment_properties_temperature_freq, String(environment_temp_freq / 1000).c_str(), true);

      //publish property attributes of humidity node
      client.publish (node_environment_properties_humidity_name,"Độ Ẩm Môi Trường",true);
      client.publish (node_environment_properties_humidity_datatype,"float",true);
      client.publish (node_environment_properties_humidity_unit,"%",true);
      client.publish (node_environment_properties_humidity_format,"20-95",true);
      client.publish (node_environment_properties_humidity_freq, String(environment_humid_freq / 1000).c_str(), true);
    }

                      /***********Device Node***********/
    //publish attributes of device node
    client.publish (node_device_name,"DoAnhDung_20181421",true); 
    client.publish (node_device_type,"SoC",true);
    client.publish (node_device_properties,"temperature,location",true);
    //publish attributes of device properties
    client.publish (node_device_properties_temperature_name,"Nhiệt Độ Thiết Bị",true);
    client.publish (node_device_properties_temperature_datatype,"float",true);
    client.publish (node_device_properties_temperature_unit,"°C",true);
    client.publish (node_device_properties_temperature_format,"60:90",true);
    client.publish(node_device_properties_temperature_freq, String(device_temp_freq / 1000).c_str(), true);

    client.publish (node_device_properties_location_name,"Vị Trí Thiết Bị",true);
    client.publish (node_device_properties_location_datatype,"GeoWithTime",true);
    client.publish (node_device_properties_location_unit,"",true);
    client.publish (node_device_properties_location_format,"",true);
    client.publish (node_device_properties_location_freq, String(device_location_freq / 1000).c_str(), true);
                      /***********Battery Node***********/
    //publish attributes of battery node
    client.publish (node_battery_name, "Điện Áp Nguồn", true);
    client.publish (node_battery_type, "Battery", true);
    client.publish (node_battery_properties, "rate_voltage",true);
    
    //publish attributes of properties
    client.publish (node_battery_properties_rate_voltage_name, "Điện áp làm việc của ắc quy", true);
    client.publish (node_battery_properties_rate_voltage_datatype, "float", true);
    client.publish (node_battery_properties_rate_voltage_unit, "voltage", true);
    client.publish (node_battery_properties_rate_voltage_format, "10.5:12.7",true);
    client.publish (node_battery_properties_rate_voltage_freq, String(battery_rate_voltage_freq).c_str(), true);
                      /***********Vehicle Node**********/
    //publish attributes of node
    client.publish (node_vehicle_name, "Mercedes BenZ", true);
    client.publish (node_vehicle_type, "Car", true);
    client.publish (node_vehicle_properties, "rpm", true);

    //publish attributes of properties
    client.publish (node_vehicle_properties_rpm_name, "Tốc Độ Vòng Tua", true);
    client.publish (node_vehicle_properties_rpm_datatype, "float", true);
    client.publish (node_vehicle_properties_rpm_unit, "rpm", true);
    client.publish (node_vehicle_properties_rpm_format, "1300:2000", true);
    client.publish (node_vehicle_properties_rpm_freq, String(vehicle_rpm_freq).c_str(), true);

    //subscribe frequency setting topic
    client.subscribe(node_environment_properties_temperatue_freq_set);
    client.subscribe(node_environment_properties_humidity_freq_set);
    client.subscribe(node_device_properties_temperature_set_freq);
    client.subscribe(node_device_properties_location_set_freq);
    client.subscribe(node_battery_properties_rate_voltage_set_freq);
    client.subscribe (node_vehicle_properties_rpm_set_freq);
    
  }
  else {
    Serial.print ("failed, rc=");
    Serial.print (client.state ());
    Serial.println (" try again in 5 seconds");
    delay (5000);
  }
}
}
void send_environment_humid()
{
  if (!isnan (newHumidity))
  {
    if (humidity != newHumidity)
    {
      humidity = newHumidity;
      Serial.println (humidity);
      client.publish (node_environment_properties_humidity_payload,String(humidity).c_str (),true);
      client.publish (device_state,"ready",true);
    }
  }
  else
  {
    Serial.println ("Failed to read humidity");
    client.publish (device_state,"ready",true);
  }
}

void send_environment_temp()
{
  if (!isnan (newTemp))
  {
    if (temp != newTemp)
    {
      temp = newTemp;
      Serial.println (temp);
      client.publish (node_environment_properties_temperature_payload,String(temp).c_str (),true);
      client.publish (device_state,"ready",true);
    }
  }
  else 
  {
    Serial.println ("Failed to read temperature");
    client.publish (device_state,"ready",true);
  }
}

void send_device_temp()
{
  //publish temperature of device
  float device_temperature = (rand() % (max_device_temperature - min_device_temperature +1)) + min_device_temperature;
  client.publish(node_device_properties_temperature_payload,String(device_temperature).c_str (),true);
  //client.publish ("mandevices/DoAnhDung_20181421/$state","ready",true);
  Serial.println("Device Temperature: ");
  Serial.println(device_temperature);
}

void send_device_location()
{
  char buffer[256] = {0};
  serializeJson(doc,buffer);
  client.publish(node_device_properties_location_payload,buffer,true);
  client.publish(device_state, "ready", true);
  Serial.println(buffer);
}

void send_rate_voltage ()
{
  float rate_voltage = rand() % (max_battery - min_battery) + min_battery;
  client.publish (node_battery_properties_rate_voltage_payload,String(rate_voltage).c_str(), true);
  client.publish (device_state, "ready", true);
  Serial.printf("rate voltage: %f \n", rate_voltage);
}

void send_rpm ()
{
  float rpm = rand() % (max_rpm - min_rpm) + min_rpm;
  client.publish (node_vehicle_properties_rpm_payload, String(rpm).c_str(),true);
  client.publish (device_state, "ready", true);
  Serial.printf("rpm: %f \n", rpm);
}

void get_geo_date_time_json()
{
  String date_time_string;
  date_time_string = iso8061_standard(dateTime);
  doc["lat"] = latitude;
  doc["lng"] = longitude;
  doc["time"] = date_time_string;
  //serializeJson(doc,Serial);
}
String iso8061_field_standard(byte field)
{
  String result = "0";
  if (field < 10)
  {
    result += String(field).c_str();
  }
  else
    return String(field).c_str();
  return result;
}

String iso8061_standard(strDateTime dateTimestruct)
{
  String result;
  String hyphen = "-";
  String colon = ":";
  String time_zone = "+07:00";
  result = String(dateTimestruct.year).c_str()
          + hyphen
          + iso8061_field_standard(dateTimestruct.month)
          + hyphen
          + iso8061_field_standard(dateTimestruct.day)
          + "T"
          + iso8061_field_standard(dateTimestruct.hour)
          + colon
          + iso8061_field_standard(dateTimestruct.minute)
          + colon
          + iso8061_field_standard(dateTimestruct.second)
          + time_zone;
  return result;
}
